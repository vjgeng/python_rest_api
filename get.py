from flask import Flask, jsonify
import random

app = Flask(__name__)

name_list = [

                {'name': "Geng"},
                {'name': "Paul"},
                {'name': "George"},
                {'name': "Paul"},
                {'name': "Mick"},
                {'name': "Keith"},
                {'name': "Chalie"}
]

result=[]
@app.route("/accounts", methods=["GET"])
def getAccounts():
    result = [(random.choice(name_list))]
    return jsonify(result)

if __name__=='__main__':
        app.run(port=8080)